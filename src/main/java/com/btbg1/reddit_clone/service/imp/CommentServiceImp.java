package com.btbg1.reddit_clone.service.imp;

import com.btbg1.reddit_clone.model.Comment;
import com.btbg1.reddit_clone.repository.CommentRepository;
import com.btbg1.reddit_clone.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImp implements CommentService {
    private CommentRepository commentRepository;

    @Autowired
    public CommentServiceImp(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    @Override
    public boolean insert(Comment comment) {
        return commentRepository.insert(comment);
    }
}
