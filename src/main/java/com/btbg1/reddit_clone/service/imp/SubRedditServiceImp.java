package com.btbg1.reddit_clone.service.imp;

import com.btbg1.reddit_clone.model.SubReddit;
import com.btbg1.reddit_clone.repository.SubRedditRepository;
import com.btbg1.reddit_clone.service.SubRedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubRedditServiceImp implements SubRedditService {

    private SubRedditRepository subRedditRepository;

    @Autowired
    public SubRedditServiceImp(SubRedditRepository subRedditRepository) {
        this.subRedditRepository = subRedditRepository;
    }

    @Override
    public List<SubReddit> findAll() {
        return subRedditRepository.findAll();
    }

    @Override
    public boolean insert(SubReddit subReddit) {
        return subRedditRepository.insert(subReddit);
    }
}
