package com.btbg1.reddit_clone.service.imp;

import com.btbg1.reddit_clone.model.Post;
import com.btbg1.reddit_clone.repository.PostRepository;
import com.btbg1.reddit_clone.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {
    private PostRepository postRepository;

    @Autowired
    public PostServiceImp(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    @Override
    public Post findOne(int id) {
        return postRepository.findOne(id);
    }

    @Override
    public boolean insert(Post post) {
        return postRepository.insert(post);
    }
}
