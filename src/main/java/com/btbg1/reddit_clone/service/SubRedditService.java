package com.btbg1.reddit_clone.service;

import com.btbg1.reddit_clone.model.SubReddit;

import java.util.List;

public interface SubRedditService {
    List<SubReddit> findAll();
    boolean insert(SubReddit subReddit);
}
