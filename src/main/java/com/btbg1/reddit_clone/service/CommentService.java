package com.btbg1.reddit_clone.service;

import com.btbg1.reddit_clone.model.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> findAll();
    boolean insert(Comment comment);
}
