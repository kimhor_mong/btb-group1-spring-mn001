package com.btbg1.reddit_clone.service;

import com.btbg1.reddit_clone.model.Post;

import java.util.List;

public interface PostService {
    List<Post> findAll();
    Post findOne(int id);
    boolean insert(Post post);
}
