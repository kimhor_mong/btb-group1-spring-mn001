package com.btbg1.reddit_clone.controller;

import com.btbg1.reddit_clone.model.Comment;
import com.btbg1.reddit_clone.service.CommentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;

@Controller
@RequestMapping("/comment")
public class CommentController {
    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping
    public List<Comment> findAll(){
        return commentService.findAll();
    }

    @PostMapping("/save-comment")
    public String addNewComment(@ModelAttribute Comment comment){
        commentService.insert(comment);
        return "redirect:/post/" + comment.getPostId();
    }
}
