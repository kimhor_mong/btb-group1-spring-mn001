package com.btbg1.reddit_clone.controller;

import com.btbg1.reddit_clone.model.Post;
import com.btbg1.reddit_clone.service.PostService;
import com.btbg1.reddit_clone.service.SubRedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
@RequestMapping("/post")
public class PostController {
    @Value("${image.path}")
    private String serverPath;

    private PostService postService;
    private SubRedditService subRedditService;

    @Autowired
    public PostController(PostService postService, SubRedditService subRedditService) {
        this.postService = postService;
        this.subRedditService = subRedditService;
    }

    @GetMapping("/{id}")
    public String showPost(@PathVariable int id, ModelMap model){
        model.addAttribute("post", postService.findOne(id));
        return "post/view";
    }

    @GetMapping("/create")
    public String showPostCreateForm(ModelMap model){
        model.addAttribute("post", new Post());
        model.addAttribute("subReddits", subRedditService.findAll());
        return "post/create-form";
    }

    @PostMapping(value ="/save-post", params = "save")
    public String addNewPost(@ModelAttribute Post post, @RequestPart(name = "file")MultipartFile multipartFile) throws IOException {
        String originalFileName = multipartFile.getOriginalFilename();
        String afterSavedFileName = UUID.randomUUID() + "." + originalFileName.substring(originalFileName.lastIndexOf(".") + 1);

        if(!multipartFile.isEmpty()){
            Files.copy(multipartFile.getInputStream(), Paths.get(serverPath + "/images/", afterSavedFileName));
            post.setImageURL(afterSavedFileName);
        }

        postService.insert(post);
        return "redirect:/";
    }

    @PostMapping(value ="/save-post", params = "discard")
    public String discardNewPost() {
        return "redirect:/";
    }
}
