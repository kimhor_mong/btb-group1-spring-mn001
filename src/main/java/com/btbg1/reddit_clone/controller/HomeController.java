package com.btbg1.reddit_clone.controller;

import com.btbg1.reddit_clone.model.Post;
import com.btbg1.reddit_clone.service.PostService;
import com.btbg1.reddit_clone.service.SubRedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class HomeController {
    private PostService postService;
    private SubRedditService subRedditService;

    @Autowired
    public HomeController(PostService postService, SubRedditService subRedditService) {
        this.postService = postService;
        this.subRedditService = subRedditService;
    }

    @GetMapping("/")
    public String findAll(Model model){
        model.addAttribute("posts", postService.findAll());
        model.addAttribute("subReddits", subRedditService.findAll());
        return "index";
    }

}
