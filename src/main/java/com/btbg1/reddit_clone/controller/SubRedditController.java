package com.btbg1.reddit_clone.controller;

import com.btbg1.reddit_clone.model.SubReddit;
import com.btbg1.reddit_clone.service.SubRedditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/subreddit")
public class SubRedditController {
    private SubRedditService subRedditService;

    @Autowired
    public SubRedditController(SubRedditService subRedditService) {
        this.subRedditService = subRedditService;
    }

    @GetMapping
    public List<SubReddit> findAll(){
        return subRedditService.findAll();
    }


    @GetMapping("/create")
    public String showCreateForm(){
        return "subreddit/create-form";
    }

    @PostMapping(value = "/save-subreddit", params = "save")
    public String addNewSubReddit(@ModelAttribute SubReddit subReddit){
        subRedditService.insert(subReddit);
        return "redirect:/";
    }

    @PostMapping(value ="/save-subreddit", params = "discard")
    public String discardNewSubReddit() {
        return "redirect:/";
    }
}
