package com.btbg1.reddit_clone.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private int id;
    private String title;
    private String description;
    private String imageURL;
    private int vote;
    private Date createdAt;
    private SubReddit subReddit;

   private List<Comment> comments;

    @JsonProperty("subReddit")
    private void unpackNested(Integer subReddit_id) {
        subReddit = new SubReddit();
        subReddit.setId(subReddit_id);
    }
}
