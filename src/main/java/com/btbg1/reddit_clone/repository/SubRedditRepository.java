package com.btbg1.reddit_clone.repository;

import com.btbg1.reddit_clone.model.Post;
import com.btbg1.reddit_clone.model.SubReddit;
import com.btbg1.reddit_clone.repository.provider.PostProvider;
import com.btbg1.reddit_clone.repository.provider.SubRedditProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubRedditRepository {
    @SelectProvider(type = SubRedditProvider.class, method = "findAll")
    @Results(id = "subRedditMapping",
            value = {
                    @Result(column = "id", property = "id"),
                    @Result(column = "title", property = "title"),
                    @Result(column = "description", property = "description"),
            })
    List<SubReddit> findAll();

    @InsertProvider(type = SubRedditProvider.class, method = "insert")
    @ResultMap("subRedditMapping")
    boolean insert(@Param("subReddit") SubReddit subReddit);
}
