package com.btbg1.reddit_clone.repository.provider;

import com.btbg1.reddit_clone.model.SubReddit;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class SubRedditProvider {
    public String findAll(){
        return new SQL(){{
            SELECT("*");
            FROM("sub_reddit");
            ORDER_BY("id desc");
        }}.toString();
    }

    public String insert(@Param("subReddit") SubReddit subReddit){
        return new SQL(){{
            INSERT_INTO("sub_reddit");
            VALUES("title, description", "#{subReddit.title}, #{subReddit.description}");
        }}.toString();
    }
}
