package com.btbg1.reddit_clone.repository.provider;

import com.btbg1.reddit_clone.model.Comment;
import com.btbg1.reddit_clone.model.SubReddit;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CommentProvider {
    public String findAll(){
        return new SQL(){{
            SELECT("c.id, c.text, c.vote, p.id postId");
            FROM("comment c");
            INNER_JOIN("post p on c.post_id = p.id");
            ORDER_BY("c.id desc");
        }}.toString();
    }

    public String insert(@Param("comment")Comment comment){
        return new SQL(){{
            INSERT_INTO("comment");
            VALUES("text, post_id", "#{comment.text}, #{comment.postId}");
        }}.toString();
    }
}
