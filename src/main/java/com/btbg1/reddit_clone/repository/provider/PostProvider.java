package com.btbg1.reddit_clone.repository.provider;

import com.btbg1.reddit_clone.model.Post;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class PostProvider {
    public String findAll(){
        return new SQL(){{
            SELECT("p.id, p.title, p.description, p.image_url, p.vote, " +
                    "p.created_at, p.sub_reddit_id, s.title as s_title, s.description as s_description");
            FROM("post p");
            INNER_JOIN("sub_reddit s on p.sub_reddit_id = s.id");
            ORDER_BY("p.id desc");
        }}.toString();
    }



    public String findOne(@Param("id") int id){

        return new SQL(){{
            SELECT("p.id, p.title, p.description, p.image_url, p.vote, " +
                    "p.created_at, p.sub_reddit_id, s.title as s_title, s.description as s_description");
            FROM("post p");
            INNER_JOIN("sub_reddit s on p.sub_reddit_id = s.id");
            WHERE("p.id = #{id}");
            ORDER_BY("p.id desc");
        }}.toString();


    }

    public String insert(@Param("post") Post post){
        return new SQL(){{
            INSERT_INTO("post");
            VALUES("title, description, image_url, sub_reddit_id","#{post.title}, #{post.description}, #{post.imageURL}, #{post.subReddit.id}" );
        }}.toString();
    }
}
