package com.btbg1.reddit_clone.repository;

import com.btbg1.reddit_clone.model.Comment;
import com.btbg1.reddit_clone.model.Post;
import com.btbg1.reddit_clone.repository.provider.PostProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

    @SelectProvider(type = PostProvider.class,method = "findAll")
    @Results(id = "postMapping",
            value = {
                    @Result(column = "id", property = "id"),
                    @Result(column = "title", property = "title"),
                    @Result(column = "description", property = "description"),
                    @Result(column = "image_url",property = "imageURL"),
                    @Result(column = "vote",property = "vote"),
                    @Result(column = "created_at", property = "createdAt"),
                    @Result(column = "sub_reddit_id",property = "subReddit.id"),
                    @Result(column = "s_title", property = "subReddit.title"),
                    @Result(column = "s_description", property = "subReddit.description"),
                    @Result(column = "id", property = "comments", javaType = List.class
                    ,many = @Many(select = "findAllCommentsByPostId"))
            })
    List<Post> findAll();

    @Select("SELECT * FROM COMMENT where comment.post_id = #{id}")
    List<Comment> findAllCommentsByPostId(int id);


    @SelectProvider(type = PostProvider.class, method = "findOne")
    @ResultMap("postMapping")
    Post findOne(@Param("id") int id);


    @InsertProvider(type = PostProvider.class, method = "insert")
    boolean insert(@Param("post") Post post);
}
