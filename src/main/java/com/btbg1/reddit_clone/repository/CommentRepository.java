package com.btbg1.reddit_clone.repository;

import com.btbg1.reddit_clone.model.Comment;
import com.btbg1.reddit_clone.model.SubReddit;
import com.btbg1.reddit_clone.repository.provider.CommentProvider;
import com.btbg1.reddit_clone.repository.provider.SubRedditProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {
    @SelectProvider(type = CommentProvider.class, method = "findAll")
    @Results(id = "commentMapping",
            value = {
                    @Result(column = "id", property = "id"),
                    @Result(column = "text", property = "text"),
                    @Result(column = "vote", property = "vote"),
                    @Result(column = "postId", property = "postId")
            })
    List<Comment> findAll();

    @InsertProvider(type = CommentProvider.class, method = "insert")
    @ResultMap("commentMapping")
    boolean insert(@Param("comment") Comment comment);
}
